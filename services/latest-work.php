		<!-- Latest Work -->
		<div id="our-work" class="team-area">
            <div class="container">
				<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                        <div class="section-headline text-center">
                            <span class="title-icon"><i class="icon icon-select"></i></span>
                            <h3>Our Latest Work In Web Development</h3>
                        </div>
                    </div>
                </div>
				<div class="row">
                    <div class="team-member">
						                        <div class="col-md-4 col-sm-6 col-xs-12 work-list">
                            <div class="single-member">
                                <div class="team-img">
                                    <a href="../portfolio/amen-inch.html">
                                        <img src="../media/2020/11/de05457cd88390a1a171a64490d66e91970.jpg" alt="amen inch">
                                    </a>
                                </div>
                                <div class="team-content text-center work-title">
                                    <h4><a href="../portfolio/amen-inch.html">amen inch</a></h4>
                                </div>
                            </div>
                        </div>
						                        <div class="col-md-4 col-sm-6 col-xs-12 work-list">
                            <div class="single-member">
                                <div class="team-img">
                                    <a href="../portfolio/shakti-security.html">
                                        <img src="../media/2020/11/22d87d84f9f5bab3179fe7548dd27701781.jpg" alt="Shakti Security">
                                    </a>
                                </div>
                                <div class="team-content text-center work-title">
                                    <h4><a href="../portfolio/shakti-security.html">Shakti Security</a></h4>
                                </div>
                            </div>
                        </div>
						                        <div class="col-md-4 col-sm-6 col-xs-12 work-list">
                            <div class="single-member">
                                <div class="team-img">
                                    <a href="../portfolio/himkarts.html">
                                        <img src="../media/2020/11/1ca6333b956504d5ae8e492391735575313.jpg" alt="HimKarts">
                                    </a>
                                </div>
                                <div class="team-content text-center work-title">
                                    <h4><a href="../portfolio/himkarts.html">HimKarts</a></h4>
                                </div>
                            </div>
                        </div>
						                        <div class="col-md-4 col-sm-6 col-xs-12 work-list">
                            <div class="single-member">
                                <div class="team-img">
                                    <a href="../portfolio/tip-top-music.html">
                                        <img src="../media/2020/08/9a26c02d51d36ecec6152e3ffaf0d93e334.jpg" alt="TIP TOP Music">
                                    </a>
                                </div>
                                <div class="team-content text-center work-title">
                                    <h4><a href="../portfolio/tip-top-music.html">TIP TOP Music</a></h4>
                                </div>
                            </div>
                        </div>
						                        <div class="col-md-4 col-sm-6 col-xs-12 work-list">
                            <div class="single-member">
                                <div class="team-img">
                                    <a href="../portfolio/real-bakers.html">
                                        <img src="../media/2020/08/1df988f85cc03c1ba90af8e298191f20972.jpg" alt="Real Bakers">
                                    </a>
                                </div>
                                <div class="team-content text-center work-title">
                                    <h4><a href="../portfolio/real-bakers.html">Real Bakers</a></h4>
                                </div>
                            </div>
                        </div>
						                        <div class="col-md-4 col-sm-6 col-xs-12 work-list">
                            <div class="single-member">
                                <div class="team-img">
                                    <a href="../portfolio/vr-infraspace.html">
                                        <img src="../media/2020/08/aa7b07ddc8088be645f6e57eb3e88aa1403.jpg" alt="VR Infraspace">
                                    </a>
                                </div>
                                <div class="team-content text-center work-title">
                                    <h4><a href="../portfolio/vr-infraspace.html">VR Infraspace</a></h4>
                                </div>
                            </div>
                        </div>
						                    </div>
                </div>
                <div class="row">
					<div class="col-md-12">
						<div class="feature-inner wow fadeInLeft" data-wow-delay="0.3s">
							<div class="feature-text">
								<div class="layer-3 wow fadeInUp text-center" data-wow-delay="0.7s">
									<a href="../portfolio.html" class="ready-btn left-btn" >View More</a>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
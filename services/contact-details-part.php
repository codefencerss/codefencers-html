		<div class="inquiry-sec">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
						<div class="icon-box">
							<div class="icon-box-icon">
								<i class="fa fa-skype"></i>
							</div>
							<div class="icon-box-info">
								<div class="icon-box-info-title">
									<span class="icon-box-sub-title">Skype ID</span>
									<hr>
									<a href="skype:raviraj_42">raviraj_42</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
						<div class="icon-box">
							<div class="icon-box-icon">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</div>
							<div class="icon-box-info">
								<div class="icon-box-info-title">
									<span class="icon-box-sub-title">Email Address</span>
									<hr>
									<a href="mailto:info@codefencers.com">info@codefencers.com</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
						<div class="icon-box">
							<div class="icon-box-icon">
								<i class="fa fa-phone" aria-hidden="true"></i>
							</div>
							<div class="icon-box-info">
								<div class="icon-box-info-title">
									<span class="icon-box-sub-title">Phone Number</span>
									<hr>
									<a href="tel:+917043225858">+91 7043 22 5858</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.5s">
						<div class="icon-box">
							<div class="icon-box-icon">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</div>
							<div class="icon-box-info">
								<div class="icon-box-info-title">
									<span class="icon-box-sub-title">Email Address</span>
									<hr>
									<a href="mailto:sales@codefencers.com">sales@codefencers.com</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Start testimonials Area -->
        <div class="reviews-area pt-50 pb-50 home-testi">
            <div class="container">
                <div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
						<div class="section-headline review-head text-center">
                            <h3>But don’t take our work for it</h3>
                            <p>See what people have to say about our services</p>
						</div>
					</div>
				</div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="Reviews-content wow fadeInUp" data-wow-delay="0.3s">
                            <!-- start testimonial carousel -->
                            <div class="testimonial-carousel2 item-indicator">
                                <div class="single-testi">
									<div class="testi-text">
										<div class="clients-text">
											<div class="row">
												<div class="col-md-7">
													<div class="client-logo"><img src="../libraries/nexus/img/client-logo/logo-2.png"></div>
													<p><b>“A next level Product Developed”</b></p>
													<p class="text-justify mt-4">As I mentioned, CodeFencers people actually developed & delivered us the next level product beyond our thinking. We were looking for just an basic Web Based DIY Menu page for our restaurants customers but when we connected with <span>Sanjay & Raviraj</span> from CodeFencers, we grown with lots of new tech. based & user friendly website with futuristic creativity added in advance, which is obviously helped by them.</p>
													<p class="mt-4">10/10 for – Hard Work / Creative Ideas / Timely delivered / Customer Support</p>
													<div class="client-text mt-20">
														<h5>Sushil Kukreti</h5>
														<p>Head – IT, Massive Restaurants Pvt. Ltd.</p>
													</div>
												</div>
												<div class="col-md-5">
													<div class="client-img">
														<img src="../libraries/nexus/img/review/shushil-kukreti.jpg">
													</div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                                <!-- End single item -->
								
								<div class="single-testi">
									<div class="testi-text">
										<div class="clients-text">
											<div class="row">
												<div class="col-md-6">
													<div class="client-logo">
														<img src="../libraries/nexus/img/client-logo/logo-6.png">
													</div>
													<p>Overall very pleased with CodeFencers Pvt. Ltd. and their friendliness with us. They did everything we asked in a timely matter. I will definitely be recommending him to other companies. Thanks for such good work.</p>
													<div class="client-text mt-20">
														<h5>Chirag Zala</h5>
														<p>Founder & Director, XL KPO Services</p>
													</div>
												</div>
												<div class="col-md-6">
													<div class="client-img">
														<img src="../libraries/nexus/img/review/chirag-zala.jpg">
													</div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End testimonials end -->
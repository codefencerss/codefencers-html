		<div class="team-area area-padding form-area" id="inquiry-part">
            <div class="container">
               <div class="row">
				<div class="single-post-comments">
					<div class="comment-respond">
						<h3 class="comment-reply-title">Inquiry Now</h3>
						<div class="col-md-6">
							<div class="center mt-50">
								<!--<h3 class="font-weight-semibold text-black text-4 mb-3 mt-4">Things we do next:</h3>
								<div class="things-list pl-3 pr-3 mb-5 pb-50">
                                    <ul class="list list-unstyled list-icons-size-2 mb-0 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" style="animation-delay: 400ms;">
                                        <li class="d-flex align-items-center text-3 text-black pb-1 mb-2"><i class="fa fa-check text-black text-2 mr-2"></i> Our representative will contact you in 24 hours</li>
                                        <li class="d-flex align-items-center text-3 text-black pb-1 mb-2"><i class="fa fa-check text-black text-2 mr-2"></i> NDA to ensure confidentiality</li>
                                        <li class="d-flex align-items-center text-3 text-black pb-1 mb-2"><i class="fa fa-check text-black text-2 mr-2"></i> We will collect project requirements</li>
                                        <li class="d-flex align-items-center text-3 text-black pb-1 mb-2"><i class="fa fa-check text-black text-2 mr-2"></i> We analyze your requirements and prepare an estimation</li>
                                        <li class="d-flex align-items-center text-3 text-black pb-1 mb-2"><i class="fa fa-check text-black text-2 mr-2"></i> Assign the project &amp; team introduction</li>
                                    </ul>
                                </div>-->
								<span class="text-center text-notes">Trusted by</span>
								<img src="http://localhost/codefencers-html/libraries/nexus/img/mix/Nexus-Client-Logos.png">
							</div>
						</div>
						<div class="col-md-6">
							<form id="contactForm" action="javascript:void(0);" onsubmit="submitInquiryForm();">
								<span class="text-right email-notes">All projects secured by NDA & IP is your's</span>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<p>Name</p>
										<input type="text" id="first-name" placeholder="First Name">
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<p>.</p>
										<input type="text" id="last-name" placeholder="Last Name">
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<p>Email</p>
										<input type="email" id="for-email" placeholder="Your Email Address">
									</div>
									<div class="col-xs-12">
										<p>Mobile No</p>
										<input type="text" id="for-phone-no" placeholder="Your Mobile No">
									</div>
									<div class="col-xs-12">
										<p>Select a Service</p>
										<select id="select-box" class="pb-10 select-service">
										  <option value="web-development">Web Development</option>
										  <option value="mobile-app-development">Mobile App</option>
										  <option value="e-commerce-development">E-Commerce</option>
										  <option value="software-development">Software Development</option>
										  <option value="corporate-branding">Corporate Branding</option>
										  <option value="digital-marketing">Digital Marketing</option>
										  <option value="other">Other</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-12">
										<p>Budget</p>
										<select id="select-box" class="pb-10 select-budget">
										  <option value=""> Select</option>
										  <option value="$1K+"> $1K+</option>
										  <option value="$5K+" selected> $5K+</option>
										  <option value="$10K+"> $10K+</option>
										  <option value="$25K+"> $25K+</option>
										  <option value="$50K+"> $50K+</option>
										  <option value="$100K+"> $100K+</option>
										  <option value="not-sure"> Not Sure</option>
										</select>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12">
										<p>How soon you want to start?</p>
										<select id="select-box" class="pb-10 select-timeline">
										  <option value=""> Select</option>
										  <option value="right-now" selected> Right Now</option>
										  <option value="in-few-week"> In Few Weeks</option>
										  <option value="in-few-week"> In Few Months</option>
										  <option value="not-sure"> Not Sure</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<p>Requirement</p>
										<select id="select-box" class="pb-10 select-requirement">
										  <option value=""> Select</option>
										  <option value="hire-dedicated-team"> Hire Dedicated Team</option>
										  <option value="new-project" selected> New Project</option>
										  <option value="existing-project"> Existing Project</option>
										  <option value="support-maintenance"> Support - Maintenance</option>
										</select>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 comment-form-comment">
										<p>Massage</p>
										<textarea id="message-box" cols="30" rows="10"></textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 comment-form-comment">
										<input class="add-btn contact-btn round-btn" type="submit" value="Get Started" style="float: left;">
										<p class="text-right" style="float: right; line-height: 88px;">100% Secure. Zero Spam.</p>
									</div>
								</div>
							</form>
						</div>
					</div>						
				</div>
			   </div>
			</div>
		</div>
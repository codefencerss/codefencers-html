		<div class="call-float-div call-close">
			<div class="inner-call-float">
			<a class="btn-call-float">
			<i class="fa fa-phone m-r-5"></i><span id="call-btn-action" class="hidden-xs call-close"> Call</span>
			</a>
			<div class="info-call">
			<div class="text-center">
			<h4 class="call-heading"> Call Us Now </h4>
			</div>
			<div class="text-center">
			<span class="phone-no"> <img src="../libraries/nexus/img/mix/india.png" alt="india" title="India"> +91 704 322 5858</span>
			</div>
			<span class="or-text-label"> OR </span>
			<form name="quickCall" id="quickCall" novalidate="novalidate" action="javascript:void(0);" onsubmit="save_callScheduler();">
			<div class="form-input-div formfieldID">
			<span class="txt-call-code-plus">+</span>
			<input type="text" name="quickNumCountry" id="quickNumCountry" class="form-control txt-call-code" maxlength="3" title="" value="91" placeholder="CC*">
			<input type="text" name="quickNum" id="quickNum" class="form-control txt-call-phone" maxlength="16" placeholder="Phone No.*">
			<input type="hidden" name="infodata" id="infodata" value="">
			<span id="errordiv" class="quickerror"></span>
			<input type="submit" name="" class="btn btn-gradient-yellow-purple btn-block m-t-15 quick-call-btn" value="Request a Call Back">
			</div>
			<div id="alertMsg" class="text-center text-white" style="display: none;"><h4 class="text-white">Thank You! </h4>We will call you back soon.</div>
			</form>
			</div>
			</div>
		</div>
		<!-- Start Footer bottom Area -->
        <footer class="footer1">
            <div class="footer-area pt-90 pb-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="footer-content">
                                <div class="footer-head">
                                    <h4>Information</h4> 
                                    <div class="footer-contacts">
										<p><span><i class="fa fa-phone"></i> :</span> +91 7043 22 5858</p>
										<p><span><i class="fa fa-envelope"></i> :</span> info@codefencers.com</p>
										<p><span><i class="fa fa-map-pin"></i> :</span> Ahmedabad - India</p>
									</div> 
                                    <div class="footer-icons">
                                        <ul>
                                            <li><a target="_blank" href="https://www.facebook.com/CodeFencers"><i class="fa fa-facebook"></i></a></li>
                                            <li><a target="_blank" href="https://twitter.com/CodeFencers"><i class="fa fa-twitter"></i></a></li>
                                            <li><a target="_blank" href="https://www.linkedin.com/company/CodeFencers."><i class="fa fa-linkedin"></i></a></li>
                                            <li><a target="_blank" href="https://www.instagram.com/CodeFencers"><i class="fa fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end single footer -->
                        <div class="col-md-5 col-sm-4 col-xs-12">
                            <div class="footer-content">
                                <div class="footer-head">
                                    <h4>Quick Links</h4>
                                    <ul class="footer-list">
                                        <li><a href="../about-us.html">About Us</a></li>
                                        <li><a href="../contact-us.html">Contact Us </a></li>
                                        <li><a href="../inquiry.html">Inquiry</a></li>
                                        <li><a href="../blog.html">Blog</a></li>
                                        <li><a href="../portfolio.html">Portfolio </a></li>
                                    </ul>
                                    <ul class="footer-list hidden-sm">
										<li><a href="javascript:void(0);">Hire Dedicated Developers</a></li>
                                        <li><a href="../partner-with-us.html">Partner With Us</a></li>
                                        <li><a href="../terms-and-conditions.html">Terms and Conditions</a></li>
                                        <!--<li><a href="privacy-policy">Privacy Policy</a></li>-->
                                        <li><a href="../sitemap">Site Map</a></li>
                                        <li><a href="../useful-links.html">Useful Links</a></li>
									</ul>
                                </div>
                            </div>
                        </div>
                        <!-- end single footer -->
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="footer-content last-content">
                                <div class="footer-head">
                                    <h4>Subscribe</h4>
									<div class="subs-feilds text-center">
                                        <form class="suscribe-input" onsubmit="addSubscriber();" action="javascript:void(0);">
                                            <input type="email" class="email form-control width-80" id="sus_email" placeholder="Type Email">
                                            <button type="submit" id="sus_submit" class="add-btn">Subscribe</button>
                                        </form>
                                    </div>
                                    <!--<div class="subs-feilds-pf text-center">
                                        <a href="portfolio" class="ready-btn left-btn"><i class="fa fa-file-pdf-o"></i> &nbsp; Company Profile</a>
                                    </div>-->
                                </div>
                            </div>
                        </div>
						<div class="col-md-12 col-sm-12 col-xs-12 pt-30">
                            <div class="footer-content">
                                <div class="footer-head">
                                    <p class="text-center">Worldwide Clients: United States (USA) | United Kingdom (UK) | Australia | United Arab Emirates | Germany | Canada.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-area-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="copyright">
                                <p>Copyright © 2020 <a href="../index.html">CodeFencers Pvt. Ltd.</a> All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
		
		<!-- all js here -->

		<!-- jquery latest version -->
		<script src="../libraries/nexus/js/vendor/jquery-1.12.4.min.js"></script>
		<!-- bootstrap js -->
		<script src="../libraries/nexus/js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="../libraries/nexus/js/owl.carousel.min.js"></script>
		<!-- Counter js -->
		<script src="../libraries/nexus/js/jquery.counterup.min.js"></script>
		<!-- waypoint js -->
		<script src="../libraries/nexus/js/waypoints.js"></script>
		<!-- magnific js -->
        <script src="../libraries/nexus/js/magnific.min.js"></script>
        <!-- wow js -->
        <script src="../libraries/nexus/js/wow.min.js"></script>
        <!-- meanmenu js -->
        <script src="../libraries/nexus/js/jquery.meanmenu.js"></script>
		<!-- Form validator js -->
		<script src="../libraries/nexus/js/form-validator.min.js"></script>
		<!-- plugins js -->
		<script src="../libraries/nexus/js/plugins.js"></script>
		<!-- main js -->
		<script src="../libraries/nexus/js/main.js"></script>
		<!-- SWEET ALERT2 -->
		<script src="../libraries/nexus/sweetalert/sweetalert2.js"></script>
		<script>
jQuery(document).ready(function () {
	setTimeout(function(){ addAnalytics(); }, 5000);
	
	/*
	$("#call-btn-action").click(function () {
		if($(this).hasClass("call-close")){
			$('.call-float-div').removeClass('call-close');
			$(this).removeClass('call-close');
		}else{
			$('.call-float-div').addClass('call-close');
			$(this).addClass('call-close');
		}
	});
	*/
	
	// CAROSEL FOR TESTIMONIAL
	$('#testimonial-slider').owlCarousel({
		loop:true,
		margin:10,
		nav:false,
		dots:false,
		interval: 1000,
		autoplay:true,
		smartSpeed:800,
		autoplayTimeout:2500,
		responsive:{
			0:{
				items:1
			},
			480:{
				items:1
			},
			768:{
				items:1
			}
		}
	})
				
	$(".btn-call-float").click(function () {
		if($('#call-btn-action').hasClass("call-close")){
			$('.call-float-div').removeClass('call-close');
			$('#call-btn-action').removeClass('call-close');
		}else{
			$('.call-float-div').addClass('call-close');
			$('#call-btn-action').addClass('call-close');
		}
	});
});

function swipeTechnologies(id = null){
	$('.home-technologies').hide();
	$('#technology-'+ id).show();
	$('.ta').removeClass('active');
	$('.ta-'+ id).addClass('active');
}

function swipeCategories(id = null){
	$('.work-list').hide();
	$('.'+ id).show();
	//$('.ta').removeClass('active');
	//$('.ta-'+ id).addClass('active');
}

var runAjaxNew = (function (i = null, ii = null, type = 'POST'){
	if(i == ''){ return; }
	ii.append('visit_from', 'web');
	ii.append('token', token);
	var ob = jQuery.ajax({
		url: i,
		type: type,
		enctype: 'multipart/form-data',
		contentType: 'application/json; charset=UTF-8',
		processData: false,
		contentType: false,
		data: ii,
		cache: false,
		async: false,
		success: function (response) {
		},
	}).responseText;
	return jQuery.parseJSON(ob);
});

function addAnalytics(){
	var data = new FormData();
	data.append('post_id', '77');
	data.append('post_type', 'page');
	data.append('page', 'services-page');
	data.append('page_title', 'Top Mobile Application Development Company In India, USA');
	data.append('b_agent', 'Mozilla/4.5 (compatible; HTTrack 3.0x; Windows 98)');
	data.append('ip_address', '157.32.148.169');
	data.append('url', 'mobile-app-development.html');
	data.append('reference', '');
	data.append('reference_url', '../index.html');
	data.append('visited_from', 1);
	data.append('event_id', 1);
	var response = runAjaxNew('restApis/add_analytics', data);
}

function addSubscriber(){
	var data = new FormData();
	data.append('sub_email', $('#sus_email').val());
	data.append('b_agent', 'Mozilla/4.5 (compatible; HTTrack 3.0x; Windows 98)');
	data.append('ip_address', '157.32.148.169');
	var response = runAjaxNew('restApis/add_subscriber', data);
	if (response.status == 'ok') {
		swal.fire({
		type: 'success',
		title: response.message,
			showConfirmButton: false,
			timer: 1500
		});
		$('#sus_email').val('');
	}else{
		swal.fire({
		type: 'error',
		title: response.message
		});
	}
}

function save_callScheduler(){
	var data = new FormData();
	data.append('country_code', $('#quickNumCountry').val());
	data.append('phone_no', $('#quickNum').val());
	data.append('b_agent', 'Mozilla/4.5 (compatible; HTTrack 3.0x; Windows 98)');
	data.append('ip_address', '157.32.148.169');
	var response = runAjaxNew('restApis/save_callScheduler', data);
	if (response.status == 'ok') {
		swal.fire({
		type: 'success',
		title: response.message,
		html: '<p>Our executive will call you as soon as possible.</p>',
		});
		$('#quickNum').val('');
	}else{
		swal.fire({
		type: 'error',
		title: response.message
		});
	}
}

function submitContactForm(){
	var data = new FormData();
	data.append('first_name', $('#first-name').val());
	data.append('last_name', $('#last-name').val());
	data.append('email', $('#for-email').val());
	data.append('phone_no', $('#for-phone-no').val());
	data.append('service', $('.select-service').val());
	data.append('budget', $('.select-budget').val());
	data.append('timeline', $('.select-timeline').val());
	data.append('requirement', $('.select-requirement').val());
	data.append('message', $('#message-box').val());
	data.append('b_agent', 'Mozilla/4.5 (compatible; HTTrack 3.0x; Windows 98)');
	data.append('ip_address', '157.32.148.169');
	var response = runAjaxNew('restApis/submit_ContactForm', data);
	if (response.status == 'ok') {
		swal.fire({
			type: 'success',
			title: response.message,
			html: '<p>Our executive will rever back you as soon as possible.</p>',
			onClose: () => {
				location.reload();
			}
		});
	}else{
		swal.fire({
		type: 'error',
		title: response.message
		});
	}
}

function submitInquiryForm(){
	var data = new FormData();
	data.append('first_name', $('#first-name').val());
	data.append('last_name', $('#last-name').val());
	data.append('email', $('#for-email').val());
	data.append('phone_no', $('#for-phone-no').val());
	data.append('service', $('.select-service').val());
	data.append('budget', $('.select-budget').val());
	data.append('timeline', $('.select-timeline').val());
	data.append('requirement', $('.select-requirement').val());
	data.append('message', $('#message-box').val());
	data.append('b_agent', 'Mozilla/4.5 (compatible; HTTrack 3.0x; Windows 98)');
	data.append('ip_address', '157.32.148.169');
	var response = runAjaxNew('restApis/submit_InquiryForm', data);
	if (response.status == 'ok') {
		swal.fire({
			type: 'success',
			title: response.message,
			html: '<p>Our executive will rever back you as soon as possible.</p>',
			onClose: () => {
				location.reload();
			}
		});
	}else{
		swal.fire({
		type: 'error',
		title: response.message
		});
	}
}

function submitPostComment(){
	var data = new FormData();
	data.append('first_name', $('#first_name').val());
	data.append('last_name', $('#last_name').val());
	data.append('email', $('#email').val());
	data.append('comment', $('#message-box').val());
	data.append('post_id', $('#post_id').val());
	data.append('b_agent', 'Mozilla/4.5 (compatible; HTTrack 3.0x; Windows 98)');
	data.append('ip_address', '157.32.148.169');
	var response = runAjaxNew('restApis/submit_postComment', data);
	if (response.status == 'ok') {
		swal.fire({
			type: 'success',
			title: response.message,
			showConfirmButton: false,
			timer: 1500
		});
		setTimeout(function(){
			location.reload();
		},2000);
	}else{
		swal.fire({
		type: 'error',
		title: response.message
		});
	}
}


function sendbulkMailForm(){
	var data = new FormData();
	data.append('subject', $('#subject').val());
	data.append('email', $('#email').val());
	data.append('message', $('#messageBody').val());
	data.append('b_agent', 'Mozilla/4.5 (compatible; HTTrack 3.0x; Windows 98)');
	data.append('ip_address', '157.32.148.169');
	var response = runAjaxNew('restApis/sendbulkMailForm', data);
	if (response.status == 'ok') {
		swal.fire({
			type: 'success',
			title: response.message,
			html: response.list,
			//showConfirmButton: false,
			//timer: 1500
		});
		setTimeout(function(){
			//location.reload();
		},2000);
	}else{
		swal.fire({
		type: 'error',
		title: response.message
		});
	}
}
</script>
</body>
</html>
		<!-- call to us sec start -->
        <div class="call-to-action">
        	<div class="container">
        		<div class="row">
        			<div class="col-md-9 col-xs-12">
        				<div class="call-action-content">
        					<h3>let us bring your idea to life..?</h3>
        				</div>
        			</div>
        			<div class="col-md-3 col-xs-12">
        				<div class="call-action-btn">
        					<a href="https://www.codefencers.com/inquiry" class="comn-btn">Call us</a>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
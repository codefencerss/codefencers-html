		<!-- why choose us sec start -->
        <div class="why-choose mt-90">
        	<div class="container">
        		<div class="row text-left">
        			<div class="col-md-12 wow fadeInUp" data-wow-delay="0.2s">
        				<h2 class="title-blue">Why Choose Us?</h2>
        				<hr class="h2-divider m-0">
        				<p class="text-left pt-10 pb-20">Apart from offering interactive and robust solutions to your business requirements, we stand right beside our client that makes us different. What’s more, here are a few more aspects that differentiate our services from others.</p>
        			</div>
        		</div>

        		<div class="row">
        			<div class="col-md-6 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
        				<div class="choose-cntnt-box">
        					<h5>Mission</h5>
        					<p>We are determined to build solutions that encourage brand growth and expansion with meeting the market needs, regardless of business size.</p>
        				</div>
        			</div>
        			<div class="col-md-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
        				<div class="choose-cntnt-box">
        					<h5>Involvement</h5>
        					<p>Effective communication is the key to successful relationships with our clients and team that eliminates potential errors.</p>
        				</div>
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-md-6 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
        				<div class="choose-cntnt-box">
        					<h5>Values</h5>
        					<p>We work more as a partner and take the responsibility to deliver high-quality products only that fulfill the ultimate goal.</p>
        				</div>
        			</div>
        			<div class="col-md-6 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
        				<div class="choose-cntnt-box">
        					<h5>Forefront Technology</h5>
        					<p>Our mobile app development firm’s versatile developers like to use forefront technology and stay updated with trends to offer smart solutions.</p>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>